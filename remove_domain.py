#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""doc
"""

import sys
import scapy.all
from scapy_http import http

def mask_domain(packets):
    """docstring for mask_domain"""
    for packet in packets:
        # Anonymize HTTP
        if packet.haslayer(http.HTTPRequest):
            print 'HTTP layer detected'
            http_layer = packet.getlayer(http.HTTPRequest)

            host = http_layer.getfieldval('Host')
            referer = http_layer.getfieldval('Referer')
            host = host.replace(DOMAIN_TO_MASK, DOMAIN_TO_USE)
            referer = referer.replace(DOMAIN_TO_MASK, DOMAIN_TO_USE)
            http_layer.setfieldval('Host', host)
            http_layer.setfieldval('Referer', referer)

        # Anonymize DNS
        # TODO: DNS response packet malformed:
        # probably an issue in the data size not maching what is announced
        if packet.haslayer('DNS'):
            print 'DNS layer detected'
            dns_layer = packet.getlayer('DNS')

            for field in dns_layer.fields:
                # Manage DNS Query
                if isinstance(dns_layer.getfieldval(field), scapy.layers.dns.DNSQR):
                    qname = dns_layer.getfieldval(field).getfieldval('qname')
                    qname = qname.replace(DOMAIN_TO_MASK, DOMAIN_TO_USE)
                    dns_layer.getfieldval(field).setfieldval('qname', qname)

                # Manage DNS Response
                if isinstance(dns_layer.getfieldval(field), scapy.layers.dns.DNSRR):
                    for i in range(dns_layer.ancount):
                        rrname = dns_layer.an[i].getfieldval('rrname')
                        rdata = dns_layer.an[i].getfieldval('rdata')
                        rrname = rrname.replace(DOMAIN_TO_MASK, DOMAIN_TO_USE)
                        rdata = rdata.replace(DOMAIN_TO_MASK, DOMAIN_TO_USE)
                        dns_layer.an[i].setfieldval('rrname', rrname)
                        dns_layer.an[i].setfieldval('rdata', rdata)

if __name__ == '__main__':
    PCAP_PATH = sys.argv[1]
    OUTPUT_PCAP = sys.argv[2]
    DOMAIN_TO_MASK = sys.argv[3]
    DOMAIN_TO_USE = 'aaaaaaaaaaaaa.fr'

    # Read pcap
    PACKETS = scapy.all.rdpcap(PCAP_PATH)
    mask_domain(PACKETS)
    # Write pcap
    scapy.all.wrpcap(OUTPUT_PCAP, PACKETS)
